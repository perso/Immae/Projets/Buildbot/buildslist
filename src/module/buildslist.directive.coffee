# Register new module
class Buildslist extends App
    constructor: -> return [
        'bbData',
        'common'
    ]

class Buildslistfield extends Directive
    constructor: ->
        return {
            replace: false
            restrict: 'E'
            scope: false
            templateUrl: "buildslist/views/buildslist.html"
            controller: '_buildslistfieldController'
        }

class _buildslistfield extends Controller
    constructor: ($scope, $location, dataService) ->
        builderid = parseInt(window.location.hash.split("/")[2])

        $scope.build = $scope.field

        $scope.build.choices = [{ "id": 0, "complete_at": 0, "name": "Fetching..."}]
        $scope.selectValue = $scope.build.choices[0]

        $scope.valueChanged = false

        $scope.changed = (newValue, byUser) ->
            if byUser?
                $scope.valueChanged = true
            if newValue?
              $scope.build.value = newValue.id
            else
              $scope.build.value = $scope.selectValue.id
        $scope.changed()

        name = (build_file) ->
          if $scope.project_name == "TestProject"
            project_name = "test"
          else
            project_name = $scope.project_name.toLowerCase()
          match = build_file.match(project_name + "_(.*).tar.gz")
          return if match && match.length == 2 then match[1] else build_file

        latest = (arr) ->
            return arr.reduce((a, b) ->
              return if a.complete_at > b.complete_at then a else b)

        fillBuilds = (builderid) ->
            builds = dataService.getBuilds(
              builderid: builderid, complete: true,
              results: [0, 1], order: "-complete_at")
            builds.onNew = (build) ->
                build.loadSteps().onNew = (step) ->
                    if step.name == "upload package"
                        if $scope.selectValue.id == 0
                            $scope.build.choices.shift()

                        elt = {
                          "complete_at": build.complete_at,
                          "name": name(step.urls[0].name),
                          "id": step.urls[0].name,
                        }

                        dup_index = $scope.build.choices.findIndex((elt_) ->
                          return (elt_.id == elt.id))
                        if dup_index == -1
                          $scope.build.choices.push(elt)
                        else if $scope.build.choices[dup_index].complete_at < elt.complete_at
                          $scope.build.choices[dup_index] = elt

                        if !$scope.valueChanged
                            $scope.selectValue = latest($scope.build.choices)
                            $scope.changed()
        findBuildBuilder = () ->
            builderid = parseInt(window.location.hash.split("/")[2])
            dataService.getBuilders(builderid).onNew = (builder) ->
                $scope.project_name = builder.name.substr(0, builder.name.lastIndexOf("_"))
                dataService.getBuilders(name: $scope.project_name + "_build").onNew = (builder_) ->
                    fillBuilds(builder_.builderid)

        findBuildBuilder()
