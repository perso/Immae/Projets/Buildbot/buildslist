from buildbot.www.plugin import Application
from buildbot.schedulers.forcesched import ChoiceStringParameter

class BuildsList(ChoiceStringParameter):
    type = "buildslist"

    def __init__(self, **kw):
        kw["strict"] = False
        super().__init__(**kw)

    def validateProperties(self, collector, properties):
        pass


ep = Application(__name__, "Buildbot builds list")
